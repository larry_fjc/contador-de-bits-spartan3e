
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity div_frec is
    Port ( clk : in  STD_LOGIC;
           clr : in  STD_LOGIC;
           sal : out  STD_LOGIC);
end div_frec;

architecture Behavioral of div_frec is

signal qs: std_logic_vector(23 downto 0);

begin
	
	process(clk, clr)
	begin
		if(clr = '1') then qs <= (others => '0');
			elsif (clk'event and clk = '1') then qs <= qs+1;
		end if;
	end process;
	sal <= qs(23);

end Behavioral;

