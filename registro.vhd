
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity registro is
    Port ( dato : in  STD_LOGIC_VECTOR (3 downto 0);
           LA : in  STD_LOGIC;
           EA : in  STD_LOGIC;
			  clr : in STD_LOGIC;
           clk : in  STD_LOGIC;
           sal : out  STD_LOGIC_VECTOR (3 downto 0));
end registro;

architecture Behavioral of registro is
signal carga : std_logic_vector(3 downto 0);
begin

	process(clk, clr)
	variable aux: bit_vector(3 downto 0);
	begin
	
		if(clr = '1') then
			carga <= "0000";
		elsif(rising_edge(clk)) then
		aux := to_bitvector(carga);
			if(LA = '1' and EA = '0') then
				carga <= dato;
			elsif(LA = '0' and EA = '1') then
				aux := aux srl 1;
				carga <= to_stdlogicvector(aux);
			elsif (LA = '0' and EA = '0') then
				carga <= carga;
			end if;
		end if;
	end process;
	sal <= carga;
end Behavioral;

