----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:34:47 11/15/2017 
-- Design Name: 
-- Module Name:    Decodificador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Decodificador is
 Port ( Entrada : in  STD_LOGIC_VECTOR (3 downto 0);
           Salida : out  STD_LOGIC_VECTOR (6 downto 0));
end Decodificador;

architecture Behavioral of Decodificador is

begin
process (Entrada) begin
		case Entrada is
			when "0000" =>  Salida <= "1111111"; -- 0
			when "0001" =>  Salida <= "0000001"; -- 1
			when "0010" =>  Salida <= "0000010"; -- 2
			when "0011" =>  Salida <= "0000011"; -- 3
			when "0100" =>  Salida <= "0000100"; -- 4
			when "0101" =>  Salida <= "0000101"; -- 5
			when "0110" =>  Salida <= "0000110"; -- 6
			when "0111" =>  Salida <= "0000111"; -- 7
			when "1000" =>  Salida <= "0001000"; -- 8 
			when others =>  Salida <= "0000000"; -- Nada
		end case;
	end process;

end Behavioral;

