library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity contador is
    Port ( dato_1 : in  STD_LOGIC_VECTOR (3 downto 0);
           LB : in  STD_LOGIC;
           EB : in  STD_LOGIC;
			  clr: in STD_LOGIC;
           clk : in  STD_LOGIC;
           sal : out  STD_LOGIC_VECTOR (3 downto 0));
end contador;

architecture Behavioral of contador is

signal carga: std_logic_vector(3 downto 0);

begin

	process(clk, clr)
	begin
		if (clr = '1') then
			carga <= "0000";
		elsif(rising_edge(clk)) then
			if (EB = '1' and LB = '0') then
				carga <= carga + "1";
			elsif (EB = '0' and LB = '1') then
				carga <= dato_1;
			elsif (EB = '0' and LB = '0') then
				carga <= carga;
			end if;
		end if;
	end process;
	sal <= carga;

end Behavioral;

