
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.paquete_p8.all;

entity main is
    Port ( dato : in  STD_LOGIC_VECTOR (3 downto 0);
           ini : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           clr : in  STD_LOGIC;
           siete_seg : out  STD_LOGIC_VECTOR (6 downto 0));
           --display : out  STD_LOGIC_VECTOR (3 downto 0));
end main;

architecture Behavioral of main is

signal set_zero	 : std_logic_vector(3 downto 0);
signal s_cont      : std_logic_vector(3 downto 0);
signal guion 		 : std_logic_vector(6 downto 0);
signal s_deco		 : std_logic_vector(6 downto 0);
signal s_dato      : std_logic_vector(3 downto 0);

signal s_la, s_ea, s_zero, s_lb, s_eb, s_ec : std_logic;

begin

	zero: process(s_dato)
	variable aux: std_logic;
	begin
		aux := s_dato(0);
		for i in 1 to 3 loop
			aux := aux or s_dato(i);
		end loop;
		s_zero <= not(aux);
	end process;

-- asignamos se�ales constantes --
guion <= "1100011";
--display <= "1000";
set_zero <= "0000";

-- hacemos los port map --

			  
entidad2: uc Port map
			( ini => ini,
			  clk => clk, 
			  clr => clr, 
			  a0 => s_dato(0), 
			  z => s_zero,
           lb => s_lb, 
			  eb => s_eb, 
			  la => s_la, 
			  ea => s_ea, 
			  ec => s_ec);
			  
entidad3: contador Port map
			( dato_1 => set_zero,
           LB => s_lb,
           EB => s_eb,
			  clr => clr,
           clk => clk,
           sal => s_cont);
			  
entidad4: registro Port map
			( dato => dato,
           LA => s_la,
           EA => s_ea,
			  clr => clr,
           clk => clk,
           sal => s_dato);

entidad5: Decodificador Port map
			( Entrada => s_cont,
           Salida => s_deco);
			  
entidad6: mux Port map
			( ent_1 => s_deco,
           ent_0 => guion,
           sel => s_ec,
           salida => siete_seg);

end Behavioral;

