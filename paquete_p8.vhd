

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package paquete_p8 is

component div_frec is
    Port ( clk : in  STD_LOGIC;
           clr : in  STD_LOGIC;
           sal : out  STD_LOGIC);
end component;

component Decodificador is
 Port ( Entrada : in  STD_LOGIC_VECTOR (3 downto 0);
           Salida : out  STD_LOGIC_VECTOR (6 downto 0));
end component;

component contador is
    Port ( dato_1 : in  STD_LOGIC_VECTOR (3 downto 0);
           LB : in  STD_LOGIC;
           EB : in  STD_LOGIC;
			  clr: in STD_LOGIC;
           clk : in  STD_LOGIC;
           sal : out  STD_LOGIC_VECTOR (3 downto 0));
end component;

component mux is
    Port ( ent_1 : in  STD_LOGIC_VECTOR (6 downto 0);
           ent_0 : in  STD_LOGIC_VECTOR (6 downto 0);
           sel : in  STD_LOGIC;
           salida : out  STD_LOGIC_VECTOR (6 downto 0));
end component;

component registro is
    Port ( dato : in  STD_LOGIC_VECTOR (3 downto 0);
           LA : in  STD_LOGIC;
           EA : in  STD_LOGIC;
			  clr : in STD_LOGIC;
           clk : in  STD_LOGIC;
           sal : out  STD_LOGIC_VECTOR (3 downto 0));
end component;


component uc is
    Port ( ini, clk, clr, a0, z : in  STD_LOGIC;
           lb, eb, la, ea, ec : out  STD_LOGIC);
end component;
 
end paquete_p8;
