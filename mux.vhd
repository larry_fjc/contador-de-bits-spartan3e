
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux is
    Port ( ent_1 : in  STD_LOGIC_VECTOR (6 downto 0);
           ent_0 : in  STD_LOGIC_VECTOR (6 downto 0);
           sel : in  STD_LOGIC;
           salida : out  STD_LOGIC_VECTOR (6 downto 0));
end mux;

architecture Behavioral of mux is

begin
	
	process(sel, ent_1, ent_0)
	begin
		if (sel = '1') then
			salida <= ent_1;
		else
			salida <= ent_0;
		end if;
	end process;

end Behavioral;

