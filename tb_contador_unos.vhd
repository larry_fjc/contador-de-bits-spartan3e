
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY tb_contador_unos IS
END tb_contador_unos;
 
ARCHITECTURE behavior OF tb_contador_unos IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT main
    PORT(
         dato : IN  std_logic_vector(3 downto 0);
         ini : IN  std_logic;
         clk : IN  std_logic;
         clr : IN  std_logic;
         siete_seg : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal dato : std_logic_vector(3 downto 0) := (others => '0');
   signal ini : std_logic := '0';
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
   signal siete_seg : std_logic_vector(6 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: main PORT MAP (
          dato => dato,
          ini => ini,
          clk => clk,
          clr => clr,
          siete_seg => siete_seg
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 20 ns;	

      -- insert stimulus here 
		dato <= "0010";
		ini <= '0';
		clr <= '1';
		
		wait for 20 ns;
		
		dato <= "0010";
		ini <= '0';
		clr <= '0';
		
		wait for 10 ns;
		
		dato <= "1010";
		ini <= '1';
		clr <= '0';
		
		wait for 50 ns;
		
		dato <= "0010";
		ini <= '0';
		clr <= '0';
		wait for 30 ns;
		
		dato <= "0111";
		ini <= '0';
		clr <= '0';
		
		wait for 30 ns;
		
		dato <= "0111";
		ini <= '1';
		clr <= '0';
	
		
      wait;
   end process;

END;
